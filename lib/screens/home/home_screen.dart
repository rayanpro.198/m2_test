import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:m2_test/app/common/constants/app_strings.dart';
import 'package:m2_test/app/services/webSocket/bloc/crypto_stream_bloc.dart';
import 'package:m2_test/screens/home/bloc/home_screen_bloc.dart';
import 'package:m2_test/screens/home/bloc/home_screen_event.dart';
import 'package:m2_test/screens/home/bloc/home_screen_state.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late HomeScreenBloc homeScreenBloc;
  late CryptoStreamBloc cryptoStreamBloc;
  StreamSubscription? _homeScreenSubscription;

  @override
  void initState() {
    super.initState();
    homeScreenBloc = context.read<HomeScreenBloc>();
    cryptoStreamBloc = context.read<CryptoStreamBloc>();

    _homeScreenSubscription = homeScreenBloc.stream.listen((state) {
      if (state is HomeScreenLoaded) {
        cryptoStreamBloc.add(ConnectToWebSocket(
            state.symbolInfoList.map((info) => info.symbol).toList()));
      }
    });

    homeScreenBloc.add(LoadSymbolInfoEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text(AppStrings.appName)),
      body: BlocConsumer<CryptoStreamBloc, CryptoStreamState>(
        listener: (context, state) {
          if (state is CryptoStreamError) {
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text('Error: ${state.message}')));
          }
        },
        builder: (context, state) {
          Widget content;
          if (state is CryptoDataUpdated) {
            var dataList = state.data.values.toList();
            content = RefreshIndicator(
              onRefresh: _handleRefresh,
              child: ListView.builder(
                itemCount: dataList.length,
                itemBuilder: (context, index) {
                  var cryptoData = dataList[index];
                  return ListTile(
                    title: Text(cryptoData.symbol),
                    subtitle: Text(
                        'Current Price: ${cryptoData.currentPrice.toStringAsFixed(4)} '
                        '| Change: ${cryptoData.priceChange.toStringAsFixed(4)}'),
                  );
                },
              ),
            );
          } else if (state is CryptoStreamError) {
            content = Center(child: Text('Error: ${state.message}'));
          } else {
            content = const Center(child: CircularProgressIndicator());
          }
          return content;
        },
      ),
    );
  }

  Future<void> _handleRefresh() async {
    (homeScreenBloc.state as HomeScreenLoaded).symbolInfoList.clear();
    homeScreenBloc.add(LoadSymbolInfoEvent());

    if (cryptoStreamBloc.state is CryptoDataUpdated) {
      cryptoStreamBloc.add(DisconnectFromWebSocket());
      final symbols = (homeScreenBloc.state as HomeScreenLoaded)
          .symbolInfoList
          .map((info) => info.symbol)
          .toList();
      cryptoStreamBloc.add(ConnectToWebSocket(symbols));
    }
  }

  @override
  void dispose() {
    _homeScreenSubscription?.cancel();
    super.dispose();
  }
}
