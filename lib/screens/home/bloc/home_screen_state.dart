import 'package:equatable/equatable.dart';
import 'package:m2_test/models/symbol_info.dart';

abstract class HomeScreenState extends Equatable {
  const HomeScreenState();

  @override
  List<Object> get props => [];
}

class HomeScreenInitial extends HomeScreenState {}

class HomeScreenLoading extends HomeScreenState {}

class HomeScreenLoaded extends HomeScreenState {
  final List<SymbolInfo> symbolInfoList;

  const HomeScreenLoaded({required this.symbolInfoList});

  @override
  List<Object> get props => [symbolInfoList];
}

class HomeScreenError extends HomeScreenState {
  final String errorMessage;

  const HomeScreenError({required this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}
