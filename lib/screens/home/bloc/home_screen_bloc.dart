import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:m2_test/app/repository/base_repo.dart';
import 'package:m2_test/screens/home/bloc/home_screen_event.dart';
import 'package:m2_test/screens/home/bloc/home_screen_state.dart';
class HomeScreenBloc extends Bloc<HomeScreenEvent, HomeScreenState> {
  final Repository repository;

  HomeScreenBloc({required this.repository}) : super(HomeScreenInitial()) {
    on<LoadSymbolInfoEvent>(_onLoadSymbolInfoEvent);
  }

  Future<void> _onLoadSymbolInfoEvent(
      LoadSymbolInfoEvent event,
      Emitter<HomeScreenState> emit,
      ) async {
    emit(HomeScreenLoading());
    try {
      final symbolInfoListEither = await repository.getSymbolInfoList();
      symbolInfoListEither.fold(
            (error) => emit(HomeScreenError(errorMessage: error)),
            (symbolList) => emit(HomeScreenLoaded(symbolInfoList: symbolList)),
      );
    } catch (e) {
      emit(const HomeScreenError(errorMessage: 'Failed to load data'));
    }
  }
}
