import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:m2_test/app/common/dependency_injection.dart';
import 'package:m2_test/app/common/navigation/page_navigation_keys.dart';
import 'package:m2_test/app/common/navigation/page_router.dart';
import 'package:m2_test/app/repository/base_repo.dart';
import 'package:m2_test/app/services/webSocket/bloc/crypto_stream_bloc.dart';

import 'screens/home/bloc/home_screen_bloc.dart';

Future<void> main() async {
  await init();
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp._internal();

  static const MyApp instanceApp = MyApp._internal();

  factory MyApp() => instanceApp;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeScreenBloc>(
          create: (context) =>
              HomeScreenBloc(repository: instance<Repository>()),
        ),
        BlocProvider<CryptoStreamBloc>(
          create: (context) => CryptoStreamBloc(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'M2',
        onGenerateRoute: PageRouter().onGenerateRoute,
        initialRoute: PageNavigationKeys.homeRoute,
        navigatorKey: PageNavigationKeys.navigatorKey,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
      ),
    );
  }
}
