import 'package:m2_test/app/utils/app_services.dart';
import 'package:m2_test/models/symbol_info.dart';

abstract class RemoteDataSource {
  Future<List<SymbolInfo>> getSymbolInfoList();
}

class RemoteDataSourceImpl implements RemoteDataSource {
  final AppService _appService;

  RemoteDataSourceImpl(this._appService);

  @override
  Future<List<SymbolInfo>> getSymbolInfoList() async {
    return await _appService.getSymbolInfoList();
  }
}
