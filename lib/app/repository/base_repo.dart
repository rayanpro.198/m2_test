import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:m2_test/app/utils/network_info.dart';
import 'package:m2_test/data/remote_data_source.dart';
import 'package:m2_test/models/symbol_info.dart';

abstract class Repository {
  Future<Either<String, List<SymbolInfo>>> getSymbolInfoList();
}

class RepositoryImpl implements Repository {
  final RemoteDataSource _remoteDataSource;
  final NetworkInfo _networkInfo;

  RepositoryImpl(this._remoteDataSource, this._networkInfo);

  @override
  Future<Either<String, List<SymbolInfo>>> getSymbolInfoList() async {
    if (await _networkInfo.isConnected) {
      // its connected to internet, its safe to call API
      try {
        final response = await _remoteDataSource.getSymbolInfoList();
        //TODO handle errors here
        // if (response.status == 200) {
        return Right(response);
        // } else {
        //   return Left(
        //       Failure(201, response.message ?? 'Unknown error'));
        // }
      } catch (error) {
        if (kDebugMode) {
          print(error);
        }
        return const Left('Unknown Error');
      }
    } else {
      // return internet connection error
      return const Left('No Internet Connection');
    }
  }
}
