import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:m2_test/app/common/constants/app_urls.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

const String applicationJson = "application/json";
const String contentType = "content-type";
const String accept = "accept";
const int apiTimeOut = 30000;

class DioFactory {
  DioFactory();

  Dio dio = Dio();

  Future<Dio> getDio() async {
    Map<String, String> headers = {
      contentType: applicationJson,
      accept: applicationJson,
    };

    dio.options = BaseOptions(
        baseUrl: AppUrls.baseUrl,
        headers: headers,
        receiveTimeout: const Duration(milliseconds: apiTimeOut),
        sendTimeout: const Duration(milliseconds: apiTimeOut));

    if (!kReleaseMode) {
      dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        compact: false,
      ));
    }

    return dio;
  }
}
