import 'package:dio/dio.dart';
import 'package:m2_test/app/common/constants/app_urls.dart';
import 'package:m2_test/models/symbol_info.dart';

abstract class AppService {
  Future<List<SymbolInfo>> getSymbolInfoList();
}

class AppServiceClient implements AppService {
  final Dio _dio;

  AppServiceClient(this._dio);

  @override
  Future<List<SymbolInfo>> getSymbolInfoList() async {
    final response = await _dio.get(AppUrls.exchangeInfo);

    final List<dynamic> data = response.data?['symbols'];

    final List<SymbolInfo> symbolInfoList = data
        .take(25) /// Limiting to the first 25 items as required
        .map((symbolData) => SymbolInfo.fromJson(symbolData))
        .toList();

    return symbolInfoList;
  }
}
