part of 'crypto_stream_bloc.dart';

abstract class CryptoStreamState {}

class CryptoStreamInitial extends CryptoStreamState {}

class CryptoDataUpdated extends CryptoStreamState {
  final Map<String, CryptoData> data;

  CryptoDataUpdated(this.data);
}

class CryptoStreamError extends CryptoStreamState {
  final String message;

  CryptoStreamError(this.message);
}

class CryptoDisconnected extends CryptoStreamState {}
