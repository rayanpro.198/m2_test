part of 'crypto_stream_bloc.dart';


abstract class CryptoStreamEvent {}
class ConnectToWebSocket extends CryptoStreamEvent {
  final List<String> symbols;
  ConnectToWebSocket(this.symbols);
}
class DisconnectFromWebSocket extends CryptoStreamEvent {}
