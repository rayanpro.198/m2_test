import 'dart:async';
import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:m2_test/app/common/constants/app_urls.dart';
import 'package:m2_test/models/crypto_data.dart';
import 'package:web_socket_channel/io.dart';

part 'crypto_stream_event.dart';
part 'crypto_stream_state.dart';

class CryptoStreamBloc extends Bloc<CryptoStreamEvent, CryptoStreamState> {
  late IOWebSocketChannel channel;
  StreamSubscription? _subscription;

  CryptoStreamBloc() : super(CryptoStreamInitial()) {
    on<ConnectToWebSocket>(_connectToWebSocket);
    on<DisconnectFromWebSocket>(_disconnectFromWebSocket);
  }

  void _connectToWebSocket(
      ConnectToWebSocket event, Emitter<CryptoStreamState> emit) async {
    final streamNames = event.symbols
        .map((symbol) => '${symbol.toLowerCase()}@ticker')
        .join('/');
    final url = '${AppUrls.webSocketUrl}=$streamNames';
    channel = IOWebSocketChannel.connect(Uri.parse(url));

    Map<String, CryptoData> cryptoDataMap = {};

    await for (final message in channel.stream) {
      try {
        final jsonData = jsonDecode(message);
        final data = jsonData['data'] as Map<String, dynamic>;
        final cryptoData = CryptoData.fromJson(data);
        cryptoDataMap[cryptoData.symbol] = cryptoData;
        emit(CryptoDataUpdated(Map<String, CryptoData>.from(cryptoDataMap)));
      } catch (e) {
        emit(CryptoStreamError(e.toString()));
      }
    }
  }

  void _disconnectFromWebSocket(
      DisconnectFromWebSocket event, Emitter<CryptoStreamState> emit) {
    _subscription?.cancel();
    channel.sink.close();
    emit(CryptoDisconnected());
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }
}
