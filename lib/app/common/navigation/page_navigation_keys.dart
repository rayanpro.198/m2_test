import 'package:flutter/material.dart';

class PageNavigationKeys {
  static const startRoute = '/';
  static const homeRoute = '/home';

  static final GlobalKey<NavigatorState> _navigatorKey =
      GlobalKey<NavigatorState>();

  static GlobalKey<NavigatorState> get navigatorKey => _navigatorKey;
}
