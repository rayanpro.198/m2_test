import 'package:flutter/material.dart';
import 'package:m2_test/app/common/navigation/page_navigation_keys.dart';
import 'package:m2_test/screens/home/home_screen.dart';

class PageRouter {
  Route? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case PageNavigationKeys.homeRoute:
        return MaterialPageRoute(
          builder: (builder) => const HomeScreen(),
        );
    }
    return null;
  }
}
