extension MapParsingExtensions on Map<String, dynamic> {
  double getDouble(String key, {double defaultValue = 0.0}) {
    var value = this[key];
    if (value == null) return defaultValue;
    return double.tryParse(value.toString()) ?? defaultValue;
  }
}
