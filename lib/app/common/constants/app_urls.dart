class AppUrls {
  static const String baseUrl = "https://www.binance.com/fapi";

  static const String webSocketUrl = "wss://fstream.binance.com/stream?streams";

  static const String exchangeInfo = '$baseUrl/v1/exchangeInfo';

}
