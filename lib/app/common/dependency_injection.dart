import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:m2_test/app/repository/base_repo.dart';
import 'package:m2_test/app/services/webSocket/bloc/crypto_stream_bloc.dart';
import 'package:m2_test/app/utils/app_services.dart';
import 'package:m2_test/app/utils/dio_factory.dart';
import 'package:m2_test/app/utils/network_info.dart';
import 'package:m2_test/data/remote_data_source.dart';
import 'package:m2_test/screens/home/bloc/home_screen_bloc.dart';

final GetIt instance = GetIt.instance;

///
/// The [init] function is responsible for adding the instances to the graph
///
Future<void> init() async {
  // Register NetworkInfo singleton
  instance.registerLazySingleton<NetworkInfo>(
    () => NetworkInfoImpl(InternetConnectionChecker()),
  );

  // Register DioFactory singleton
  instance.registerLazySingleton<DioFactory>(() => DioFactory());

  // Register AppService singleton
  Dio dio = await instance<DioFactory>().getDio();
  instance.registerLazySingleton<AppService>(() => AppServiceClient(dio));

  // Register RemoteDataSource singleton
  instance.registerLazySingleton<RemoteDataSource>(
    () => RemoteDataSourceImpl(instance<AppService>()),
  );

  // Register Repository singleton
  instance.registerLazySingleton<Repository>(
    () => RepositoryImpl(
      instance<RemoteDataSource>(),
      instance<NetworkInfo>(),
    ),
  );

  // Register HomeScreenBloc factory with Repository dependency
  instance.registerFactory(
      () => HomeScreenBloc(repository: instance<Repository>()));

  // Register CryptoStreamBloc factory with Repository dependency
  instance.registerFactory(() => CryptoStreamBloc());
}
