import 'package:json_annotation/json_annotation.dart';
import 'package:m2_test/app/common/extensions/map_extensions.dart';
part 'crypto_data.g.dart';

@JsonSerializable()
class CryptoData {
  final String symbol;
  final double currentPrice;
  final double priceChange;

  CryptoData({
    required this.symbol,
    required this.currentPrice,
    required this.priceChange,
  });

  factory CryptoData.fromJson(Map<String, dynamic> json) {
    return CryptoData(
      symbol: json['s'] as String? ?? 'Unknown',
      currentPrice: json.getDouble('c'),
      priceChange: json.getDouble('p'),
    );
  }

  Map<String, dynamic> toJson() => _$CryptoDataToJson(this);
}
