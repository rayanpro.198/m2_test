// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'crypto_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CryptoData _$CryptoDataFromJson(Map<String, dynamic> json) => CryptoData(
      symbol: json['symbol'] as String,
      currentPrice: (json['currentPrice'] as num).toDouble(),
      priceChange: (json['priceChange'] as num).toDouble(),
    );

Map<String, dynamic> _$CryptoDataToJson(CryptoData instance) =>
    <String, dynamic>{
      'symbol': instance.symbol,
      'currentPrice': instance.currentPrice,
      'priceChange': instance.priceChange,
    };
