// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'symbol_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SymbolInfo _$SymbolInfoFromJson(Map<String, dynamic> json) => SymbolInfo(
      symbol: json['symbol'] as String,
      baseAsset: json['baseAsset'] as String,
      quoteAsset: json['quoteAsset'] as String,
      status: json['status'] as String,
      pricePrecision: json['pricePrecision'] as int,
      quantityPrecision: json['quantityPrecision'] as int,
    );

Map<String, dynamic> _$SymbolInfoToJson(SymbolInfo instance) =>
    <String, dynamic>{
      'symbol': instance.symbol,
      'baseAsset': instance.baseAsset,
      'quoteAsset': instance.quoteAsset,
      'status': instance.status,
      'pricePrecision': instance.pricePrecision,
      'quantityPrecision': instance.quantityPrecision,
    };
