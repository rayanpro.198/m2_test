import 'package:json_annotation/json_annotation.dart';

part 'symbol_info.g.dart';

@JsonSerializable()
class SymbolInfo {
  final String symbol;
  final String baseAsset;
  final String quoteAsset;
  final String status;
  final int pricePrecision;
  final int quantityPrecision;

  SymbolInfo({
    required this.symbol,
    required this.baseAsset,
    required this.quoteAsset,
    required this.status,
    required this.pricePrecision,
    required this.quantityPrecision,
  });

  factory SymbolInfo.fromJson(Map<String, dynamic> json) => _$SymbolInfoFromJson(json);

  Map<String, dynamic> toJson() => _$SymbolInfoToJson(this);
}